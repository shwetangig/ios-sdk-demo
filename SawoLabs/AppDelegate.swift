//
//  AppDelegate.swift
//  SawoLabs
//
//  Created by Rhytthm Mahajan on 28/05/21.
//375e209c-4adc-4213-96cc-8bc6cc887141


import UIKit
import OneSignal

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, OSSubscriptionObserver {
    
    var window: UIWindow?
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void)
    {
        // Do whatever you want when the user tapped on a notification
        // If you are waiting for specific data from the notification
        // (e.g., key: "target" and associated with "value"),
        // you can capture it as follows then do the navigation:

        // You may print `userInfo` dictionary, to see all data received with the notification.
//        let userInfo = response.notification.request.content.userInfo
//        print(userInfo)
//        if let targetValue = userInfo["target"] as? String, targetValue == "value"
//        {
//            coordinateToSomeVC()
//        }
        
        completionHandler()
    }

    private func coordinateToSomeVC()
    {
        guard let window = UIApplication.shared.keyWindow else { return }

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let yourVC = storyboard.instantiateViewController(identifier: "TrustedDeviceView")
        
        let navController = UINavigationController(rootViewController: yourVC)
        navController.modalPresentationStyle = .fullScreen

        // you can assign your vc directly or push it in navigation stack as follows:
        window.rootViewController = navController
        window.makeKeyAndVisible()
    }


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        application.delegate = self

        NetworkMonitor.shared.startMonitoring()
        
        // Remove this method to stop OneSignal Debugging
          OneSignal.add(self as OSSubscriptionObserver)
          OneSignal.setLogLevel(.LL_VERBOSE, visualLevel: .LL_NONE)

          // OneSignal initialization
          OneSignal.initWithLaunchOptions(launchOptions)
          OneSignal.setAppId("375e209c-4adc-4213-96cc-8bc6cc887141")
           

          OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
            let notificationReceivedBlock: OSNotificationWillShowInForegroundBlock = { notification,OSNotificationDisplayResponse  in

                print("Received Notification: \(notification.rawPayload)")
                print("OSNotificationOpenedResult- \(OSNotificationOpenedResult.self)")
            }

            
          })
        
                
                

        return true
    }
    func onOSSubscriptionChanged(_ stateChanges: OSSubscriptionStateChanges) {
       if !stateChanges.from.isSubscribed && stateChanges.to.isSubscribed {
          print("Subscribed for OneSignal push notifications!")
          // get player ID
          stateChanges.to.userId
       }
       print("SubscriptionStateChange: \n\(stateChanges)")
    }

    // MARK: UISceneSession Lifecycle

//    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
//        // Called when a new scene session is being created.
//        // Use this method to select a configuration to create the new scene with.
//        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
//    }
//
//    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
//        // Called when the user discards a scene session.
//        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
//        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
//    }

    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("iOS Native didReceiveRemoteNotification: ", userInfo.debugDescription)
        
        switch UIApplication.shared.applicationState {
        case .active:
            handleNotificationPayload(userInfo: userInfo)
            //app is currently active, can update badges count here
            break
        case .inactive:
            //app is transitioning from background to foreground (user taps notification), do what you need when user taps here
            break
        case .background:
            //app is in background, if content-available key of your notification is set to 1, poll to your backend to retrieve data and update your interface here
            break
        default:
            break
        }
        
        
        
        // This block gets called when the user reacts to a notification received
        let timeInterval = Int(NSDate().timeIntervalSince1970)
        OneSignal.sendTags(["last_push_received": timeInterval])
        
        print("badge number: ", UIApplication.shared.applicationIconBadgeNumber.description)
    }
    
    func handleNotificationPayload(userInfo: [AnyHashable : Any]) {
        if let customOSPayload = userInfo["custom"] as? NSDictionary {
            if let additionalData = customOSPayload["a"] as? NSDictionary {
                print("additionalData: ", additionalData)
                
                let secondary_device_brand = additionalData["secondary_device_brand"] as? String ?? ""
                let secondary_device_id = additionalData["secondary_id"] as? String ?? ""
                let trusted_id = additionalData["trusted_id"] as? String ?? ""
                
                print("Device Brand: \(secondary_device_brand) DeviceId: \(secondary_device_id) trustedId: \(trusted_id)")
                
                guard let userDefaults = UserDefaults(suiteName: "group.Trusted.SawoLabs.onesignal") else {
                    print("No user defaults found !!!!!!!! ")
                    return
                    
                }
                userDefaults.set("\(secondary_device_brand)", forKey: "secondary_device_brand")
                userDefaults.set("\(secondary_device_id)", forKey: "secondary_device_id")
                userDefaults.set("\(trusted_id)", forKey: "trusted_id")
                userDefaults.synchronize()
                routeToTrustedDevice()
                
            }
            if let notificationId = customOSPayload["i"] {
                print("notificationId: ", notificationId)
            }
            if let launchUrl = customOSPayload["u"] {
                print("launchUrl: ", launchUrl)
            }
        }
        if let aps = userInfo["aps"] as? NSDictionary {
            if let alert = aps["alert"] as? NSDictionary {
                if let messageBody = alert["body"] {
                    print("messageBody: ", messageBody)
                }
                if let messageTitle = alert["title"] {
                    print("messageTitle: ", messageTitle)
                }
            }
        }
    }
    
    func routeToTrustedDevice() {
        
        var secondary_device_brand_name = ""
//        var VCname = ""
        
        if let userDefaults = UserDefaults(suiteName: "group.Trusted.SawoLabs.onesignal") {
            let secondary_device_brand = userDefaults.string(forKey: "secondary_device_brand")
            print("Scene Delegate Payload check - \(secondary_device_brand ?? "")")
            secondary_device_brand_name = "\(secondary_device_brand ?? "")"
            
        }

        if(secondary_device_brand_name != ""){
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TrustedDeviceView")
            self.window?.rootViewController = vc
            self.window?.makeKeyAndVisible()
        }
            
        

        //window = UIWindow(windowScene: windowScene)
                
       
        
        
        
//        let window = UIWindow(frame:UIScreen.main.bounds)
//        let rootViewController = window.rootViewController as! UINavigationController
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//
//        let vc: TrustedDeviceViewController = storyboard.instantiateViewController(withIdentifier: "TrustedDeviceView") as! TrustedDeviceViewController
//        rootViewController.pushViewController(vc, animated: true)
        print("Trusted VC has been show ------->  ")
    }
    
 
}



