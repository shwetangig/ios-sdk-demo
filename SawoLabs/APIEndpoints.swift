//
//  APIEndpoints.swift
//  SawoLabs
//
//  Created by Shwetangi Gurav on 20/10/21.
//

import Foundation

let apiVersion = "v1"
let apiDomain: String = "https://sawo-api.herokuapp.com/api"


struct APIEndpoints {
    static let kRegisterDevice = "\(apiDomain)/\(apiVersion)/register_device/"
    static let kSecondaryTrustedDevice =  "\(apiDomain)/\(apiVersion)/secondary_trusted_device/"
}
