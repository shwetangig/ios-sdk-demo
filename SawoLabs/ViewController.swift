//
//  ViewController.swift
//  SawoLabs
//
//  Created by Rhytthm Mahajan on 28/05/21.
//

import UIKit
import WebKit
import LocalAuthentication
import SwiftKeychainWrapper
import OneSignal
import SwiftyJSON

class ViewController: UIViewController,WKNavigationDelegate {
    
    var webView: WKWebView!
    //    var keyValue = ""
    
    var publicKEY = ""
    var privateKEY = ""
    var sessionID = ""
    
    //    var keychainPublicKEY = KeychainWrapper.standard.string(forKey: "publicKEY")
    //    var keychainPrivateKEY = KeychainWrapper.standard.string(forKey: "privateKEY")
    //    var keychainSessionID = KeychainWrapper.standard.string(forKey: "sessionID")
    
    var userId = ""
    var pushToken = ""
    var subscribed = false
    var name_device = ""
    var device_version = ""
    var device_model = ""
    
    // MARK: - : Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addObservers()
        fetchOneSignalDeviceState()
        
        // Tried Json Serialization but it didnt work
        let session = URLSession.shared
        let url = APIEndpoints.kRegisterDevice
        
        let request = NSMutableURLRequest(url: NSURL(string: url)! as URL)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let params :[String: Any] = ["device_brand": "Apple",
                                     "device_name": "\(name_device)",
                                     "device_model": "\(device_model)",
                                     "device_id": "\(userId)",
                                     "device_token": "\(pushToken)",
                                     "sdk_variant": "ios"]
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions())
            let task = session.dataTask(with: request as URLRequest as URLRequest, completionHandler: {(data, response, error) in
                
                if let response = response {
                    let nsHTTPResponse = response as! HTTPURLResponse
                    let statusCode = nsHTTPResponse.statusCode
                    print ("status code for post api register (<___<>___>)= \(statusCode)")
                }
                if let error = error {
                    print ("\(error)")
                }
                if let data = data {
                    do{
                        let jsonResponse = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions())
                        print ("data = \(jsonResponse)")
                    }catch _ {
                        print ("OOps not good JSON formatted response")
                    }
                }
            })
            task.resume()
        } catch _ {
            print ("Oops something happened")
        }
        
        guard let NotificationResponse = UserDefaults.standard.string(forKey: SharedUserDefaults.keys.NotificationReceived) else { return }
        if NotificationResponse == "Yes" {
            print("response Received")
        }
    }
    
    func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(NotificationTapped), name: Notification.Name("NotificationTapped"), object: nil)
    }
    
    func fetchOneSignalDeviceState() {
        if let deviceState = OneSignal.getDeviceState() {
            userId = deviceState.userId ??  ""
            pushToken = deviceState.pushToken ?? ""
            subscribed = deviceState.isSubscribed
            name_device = UIDevice.current.name
            device_version = UIDevice.current.systemVersion
            device_model = UIDevice.current.modelName
        }
        
        let userDefaults = UserDefaults(suiteName: "group.Trusted.SawoLabs.onesignal")!
        userDefaults.set("\(String(userId))", forKey: "device_id")
        userDefaults.synchronize()
        
        print(name_device)
        print(device_version)
        print(device_model)
        print("userID - \(String(describing: userId)), Push Token - \(pushToken), subscribed - \(subscribed)")
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        guard let userDefaults = UserDefaults(suiteName: "group.Trusted.SawoLabs.onesignal") else { return }
        
        let public_key = userDefaults.string(forKey: "PUBLIC KEY")
        let private_key = userDefaults.string(forKey: "PRIVATE KEY")
        let session_id = userDefaults.string(forKey: "SESSION ID")
        
        print("\(public_key)  ---------> \(private_key) ---------> \(session_id)")

        
        if let customPayload = userDefaults.object(forKey: "PushNotificationPayload") as? NSDictionary,
           let payload = customPayload["a"] as? NSDictionary {
            
            print("One signal Custom Payload - \(payload)")

            let secondary_device_brand = payload["secondary_device_brand"] as? String ?? ""
            let secondary_device_id = payload["secondary_device_id"] as? String ?? ""
            let trusted_id = payload["trusted_id"] as? String ?? ""
            
            print("Device Brand: \(secondary_device_brand) DeviceId: \(secondary_device_id) trustedId: \(trusted_id)")
            
            
            userDefaults.set("\(secondary_device_brand)", forKey: "secondary_device_brand")
            userDefaults.set("\(secondary_device_id)", forKey: "secondary_device_id")
            userDefaults.set("\(trusted_id)", forKey: "trusted_id")
            userDefaults.synchronize()
        }
        
    }
    
    @IBAction func LoginButtonPressed(_ sender: Any) {
        
        super.viewWillAppear(true)
        //webView.navigationDelegate = self
        
        
        self.setupWebView()
        if NetworkMonitor.shared.isConnected{
            print("User is not Connected to Internet")
            webView.removeFromSuperview()
        }
        else{
            print("User is Connected to Internet")
            
        }
        
        
        self.view.addSubview(self.webView)
        let Key = "4324a93f-cb37-45ec-84de-687bb4dca1af"
        //470769a5-16a2-4037-a6d3-bf2cc5e23429
        let Identifiers = "email"     // email
        let Secret = "6048909b0946b1107406f8f8YaPK5qiPbRefssuR1LBMH7TN"
        // 5f7d572457aabffaf3b5a4cejyO3VwgmX8MY2qQOay1qp0NM
        let Url = "https://sawo-sdk-browser.herokuapp.com/?apiKey=\(Key)&identifierType=\(Identifiers)&webSDKVariant=ios&apiKeySecret=\(Secret)"
        print(Url)
        
        if let url = URL(string: Url) {
            let request = URLRequest(url: url)
            self.webView.load(request)
            
        }
        print(Key)
        print(Identifiers)
        //        print("keychainPublicKEY is \(keychainPublicKEY ?? "not found")")
        //        print("keychainPrivateKEY is \(keychainPrivateKEY ?? "not found")")
        //        print("keychainSessionID is \(keychainSessionID ?? "not found")")
        
        
    }
    
    // MARK: - : Private methods
    
    private func setupWebView() {
        var script = ""
        let scriptTrusted =
        """
        window.webSDKInterface = {
            getPlayerID() {
                return "\(userId)"
            }
        }
        """
        print(scriptTrusted)
        
        if publicKEY.isEmpty && privateKEY.isEmpty {
            print("keyValue is empty.")
            script =  """
                      window.test = function() {return false}
                      """
        } else {
            print("keyValue is not empty.")
            script =  """
                      window.test = function() {return true}
                      """
        }
        
        let userScript = WKUserScript(source: script, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
        let userScriptTrusted = WKUserScript(source: scriptTrusted, injectionTime: .atDocumentStart, forMainFrameOnly: true)
        
        let contentController = WKUserContentController()
        contentController.addUserScript(userScript)
        contentController.addUserScript(userScriptTrusted)
        contentController.add(self, name: "payloadCallback")
        
        
        let config = WKWebViewConfiguration()
        config.userContentController = contentController
        self.webView = WKWebView(frame: self.view.bounds, configuration: config)
        
    }
    
    @objc func NotificationTapped(notification: NSNotification) {
        
        DispatchQueue.main.async {
            //Route to TrustedViewController
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc: TrustedDeviceViewController = storyboard.instantiateViewController(withIdentifier: "TrustedDeviceView") as! TrustedDeviceViewController
            self.navigationController?.pushViewController(vc, animated: true)
            print("Trusted VC has been show ------->  ")
        }
    }
    
}


extension ViewController: WKScriptMessageHandler {
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        
        if message.name == "payloadCallback" {
            print("JavaScript is sending a message \(message.body)")
            let s = String(describing: message.body)
            
            let payloadWords = "user_id"
            let payloadSuccessful = s.contains(payloadWords)
            
            var buttonClicked = false
            var publicKey = ""
            var privateKey = ""
            var sessionId = ""
            var testValue = false
            
            
            if let bodyString = message.body as? String {
                let data = Data(bodyString.utf8)
                do {
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                        if let keys = json["keys"] as? [String: Any] {
                            privateKey = keys["privateKey"] as? String ?? ""
                            publicKey = keys["publicKey"] as? String ?? ""
                        }
                        
                        buttonClicked = json["is_login_button_clicked"] as? Bool ?? false
                        sessionId = json["sessionID"] as? String ?? ""
                        testValue = json["testValue"] as? Bool ?? false
                    }
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                }
            }
            
            
            if buttonClicked {
                let context:LAContext = LAContext()
                if context.canEvaluatePolicy(.deviceOwnerAuthentication, error: nil){
                    context.evaluatePolicy(LAPolicy.deviceOwnerAuthentication, localizedReason: "Message") { (good, error) in
                        if good {
                            print("good")
                            self.publicKEY = String(publicKey)
                            self.privateKEY = String(privateKey)
                            self.sessionID = String(sessionId)
                            
                            //                            KeychainWrapper.standard.set(self.publicKEY, forKey: "publicKEY")
                            //                            KeychainWrapper.standard.set(self.privateKEY, forKey: "privateKEY")
                            //                            KeychainWrapper.standard.set(self.sessionID, forKey: "sessionID")
                            
                            let userDefaults = UserDefaults(suiteName: "group.Trusted.SawoLabs.onesignal")!
                            userDefaults.set("\(self.publicKEY)", forKey: "PUBLIC KEY")
                            userDefaults.set("\(self.privateKEY)", forKey: "PRIVATE KEY")
                            userDefaults.set("\(self.sessionID)", forKey: "SESSION ID")
                            userDefaults.synchronize()
                            
                            
                        } else {
                            print("try again")
                            DispatchQueue.main.async {
                                self.webView.reload()
                            }
                            
                        }
                    }
                }
            }
            if payloadSuccessful {
                NotificationCenter.default.post(name: Notification.Name("LoginApproved"), object: nil)
                let Payload = ["UserPayload": s]
                NotificationCenter.default.post(name: Notification.Name("PayloadOfUser"), object: nil,userInfo: Payload)
                print("payload was successfully reached")
            }
            
        }
    }
}


