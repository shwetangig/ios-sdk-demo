//
//  SharedUserDefaults.swift
//  SawoLabs
//
//  Created by Rhytthm Mahajan on 31/05/21.
//

import Foundation

struct SharedUserDefaults {
    static let suiteName = "group.Trusted.SawoLabs.onesignal"
    struct keys {
        static let NotificationReceived = ""
        static let message = ""
        static var pushNotificationPayload = ""
    }
}
