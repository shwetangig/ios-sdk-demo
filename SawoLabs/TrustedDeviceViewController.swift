//
//  TrustedDeviceViewController.swift
//  TrustedDeviceViewController
//
//  Created by Rhytthm Mahajan on 10/09/21.
//

import UIKit

class TrustedDeviceViewController: UIViewController {
    
    @IBOutlet weak var SecondaryDeviceLabel: UILabel!
    
    var LabelSec = ""
    var SecondaryDevice = "Secondary Device..."
    var SecondaryDeviceId = ""
    var TrustedId = ""
    var DeviceId = ""
    
    // MARK: - : Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        guard let userDefaults = UserDefaults(suiteName: "group.Trusted.SawoLabs.onesignal") else { return }
        
        SecondaryDevice = userDefaults.string(forKey: "secondary_device_brand") ?? ""
        SecondaryDeviceId = userDefaults.string(forKey: "secondary_device_id") ?? ""
        TrustedId = userDefaults.string(forKey: "trusted_id") ?? ""
        DeviceId = userDefaults.string(forKey: "device_id") ?? ""
        
        print("SecondaryDevice - \(SecondaryDevice)")
        print("SecondaryDeviceId - \(SecondaryDeviceId)")
        print("TrustedId - \(TrustedId)")
        print("DeviceId - \(DeviceId)")
        
    }
    
    @IBAction func DeclineAction(_ sender: Any) {
    }
    
    @IBAction func AcceptAction(_ sender: Any) {
        
        let session = URLSession.shared
        let url = APIEndpoints.kSecondaryTrustedDevice
        let request = NSMutableURLRequest(url: NSURL(string: url)! as URL)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        let params :[String: Any] = ["trusted_id": TrustedId,
                                     "secondary_id": SecondaryDeviceId,
                                     "device_id": DeviceId,
                                     "trusted_response": "allowed"]
        
        print("TrustedID - \(TrustedId)")
        //        print("TrustedID.dropLast- \(TrustedId.dropLast())")
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions())
            let task = session.dataTask(with: request as URLRequest as URLRequest, completionHandler: {(data, response, error) in
                if let response = response {
                    let nsHTTPResponse = response as! HTTPURLResponse
                    let statusCode = nsHTTPResponse.statusCode
                    print ("status code for 2nd post api register (                                       )= \(statusCode)")
                }
                if let error = error {
                    print ("\(error)")
                }
                if let data = data {
                    do {
                        let jsonResponse = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions())
                        print ("data = \(jsonResponse)")
                    } catch _ {
                        print ("OOps not good JSON formatted response")
                    }
                }
            })
            task.resume()
        } catch _ {
            print ("Oops something happened")
        }
        resetUserDefaults()
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.present(newViewController, animated: true, completion: nil)
        
    }
    
    func resetUserDefaults() {
        let userDefaults = UserDefaults(suiteName: "group.Trusted.SawoLabs.onesignal")!
        userDefaults.set("", forKey: "secondary_device_brand")
        userDefaults.set("", forKey: "PushNotificationPayload")
        userDefaults.set("", forKey: "secondary_device_id")
        userDefaults.set("", forKey: "trusted_id")
        userDefaults.synchronize()
        
    }
    
    
}
